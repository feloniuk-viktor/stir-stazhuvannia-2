$(function (){
    // $('.slider').slick({
    //     infinite: false,
    //     dots: true,
    //     slidesToShow: 3,
    //     slidesToScroll: 1
    // });

    $('.section_two_slider').slick({
        dots: true,
        centerMode: true,
        arrows: true,
        infinite:true,
        centerPadding: '60px',
        slidesToShow: 3,

        // prevArrow: '<img src="../pictures/section_two/left.png">',
        // nextArrow: '<img src="../pictures/section_two/right.png">',

        prevArrow: $('#sl-prev'),
        nextArrow: $('#sl-next'),


        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    //
    // $('.sl_prev').click('click', function() {
    //     $('section_two_slider').slick('slickNext');
    // });

})
